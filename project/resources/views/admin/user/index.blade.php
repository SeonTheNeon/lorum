@extends("admin.layout")

@section("content")
    <div class="input-group">
        <span class="input-group-addon">@</span>
        <input id="player-name-input" type="text" class="form-control typeahead" placeholder="Username">
        <span class="input-group-btn">
            <button class="btn btn-success" id="btn-search">Search!</button>
        </span>
    </div>
    <table class="table table-default">
        <thead>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Action</td>
            </tr>
        </thead>
        <tbody id="table-body">
            @foreach($users as $user)
                <tr>
                    <td><img src="{!! $user->avatar!!}" class="img-circle"></td>
                    <td>{!! $user->personaname() !!}</td>
                    <td>
                        <a class="btn btn-info" href="{{URL::to('/admin/users/view/'.$user->steamid64)}}">View</a>
                        <a class="btn btn-warning" href="{{URL::to('/admin/users/edit/'.$user->steamid64)}}">Edit</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div>
        <button class="btn btn-success" id="load-more-button"> Load More </button>
    </div>
    <script>
        var currentlyWorking = false;
        var curPage = 0;
        var tableMoreTemplate = null;
        var suggestionTemplate = null;
        var playersBloodhound = null;
        var toSearch = null;

        function search() {
            if(currentlyWorking)
                return;
            curPage = 0;
            currentlyWorking = true;
            $("#load-more-button").attr('disabled', 'disabled');
            $("#btn-search").attr('disabled', 'disabled');
            toSearch = $('#player-name-input').val();

            $.getJSON('/admin/users/infinite', {page: 0, word: toSearch}, function(data) {
                var output = tableMoreTemplate({users: data});
                $("#table-body").html(output);
                if(data.length === 10) {
                    $("#load-more-button").removeAttr('disabled');
                    $("#btn-search").removeAttr('disabled');
                    currentlyWorking = false;
                } else if( data.length < 10 ) {
                    currentlyWorking = true;
                    $("#load-more-button").attr('disabled', 'disabled');
                    $("#btn-search").attr('disabled', 'disabled');
                }
            });
        }

        function updateSearch() {
            if(currentlyWorking)
                return;
            currentlyWorking = true;
            $("#load-more-button").attr('disabled', 'disabled');
            curPage++;
            $.getJSON('/admin/users/infinite', {page: curPage, word: toSearch}, function(data) {
                var output = tableMoreTemplate({users: data});
                $("#table-body").append(output);
                if(data.length === 10) {
                    currentlyWorking = false;
                    $("#load-more-button").removeAttr('disabled');
                } else if( data.length < 10 ) {
                    currentlyWorking = true;
                    $("#load-more-button").attr('disabled', 'disabled');
                    $("#btn-search").attr('disabled', 'disabled');
                }
            });
        }

        $(function() {
            $('#load-more-button').on('click', function() {
                updateSearch();
            });

            $('#btn-search').on('click', function() {
                search();
            });

            var source = $("#table-more-template").html();
            tableMoreTemplate = Handlebars.compile(source);
        });
    </script>
    @include("admin.user.handlebars_index")
@endsection
