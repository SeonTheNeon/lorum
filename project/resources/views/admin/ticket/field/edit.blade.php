@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/ticket/type/{$ticketType->id}/field/{$ticketTypeField->id}/edit")}}" method="post">
        <div class="form-group">
            <label for="name">Type Field Name:</label>
            <input type="text" name="name" id="name" placeholder="Ban Disputes" class="form-control" value="{{$ticketTypeField->name}}" required />
        </div>
        <div class="form-group">
            <label for="description">Type Field Description:</label>
            <textarea name="description" id="description" rows="10" cols="80">
                {{$ticketTypeField->description}}
            </textarea>
        </div>
        <div class="form-group">
            <label for="type">Type</label>
            <select name="type" id="type" class="form-control" required>
                @foreach(array("string", "textarea", "integer", "date", "password", "steamid", "serverid", "format_view") as $item)
                    <option value="{{$item}}" @if($item === $ticketTypeField->type) selected @endif >{{$item}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="order">Order</label>
            <input type="number" name="order" id="order" placeholder="42" class="form-control" value="{{$ticketTypeField->order}}" required />
        </div>
        <input type="submit" class="btn btn-success" />
        <a href="{{URL::to("/admin/ticket/type/{$ticketType->id}/edit")}}" class="btn btn-info">Back</a>
    </form>

    <script>
        jQuery(function() {
            CKEDITOR.replace("description");
        });
    </script>
@endsection
