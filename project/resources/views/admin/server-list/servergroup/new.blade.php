@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/server-list/servergroup")}}" method="post">
        <div class="form-group">
            <label for="name">Group Name:</label>
            <input type="text" name="name" id="name" placeholder="Cheeky Server Group 1234" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="name">Show a spoiler instead of all servers?</label>
            <input type="checkbox" name="spoilers" id="spoilers" class="form-control" />
        </div>
        <input type="submit" class="btn btn-success" />
    </form>
@endsection