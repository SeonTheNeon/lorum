@extends("admin.layout")

@section("content")
    <form action="{{URL::to("/admin/server-list/server")}}" method="post">
        <div class="form-group">
            <label for="name">Server Name:</label>
            <input type="text" name="name" id="name" placeholder="Cheeky Server 1234" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="ip">IP:</label>
            <input type="text" name="ip" id="ip" placeholder="XXX.XXX.XXX.XXX" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="port">Port:</label>
            <input type="number" name="port" id="port" placeholder="27015" class="form-control" required />
        </div>
        <div class="form-group">
            <label for="server_group_id"></label>
            <select name="server_group_id" id="server_group_id" class="form-control" required>
                @foreach($servergroups as $servergroup)
                    <option value="{{$servergroup->id}}">{{$servergroup->name}}</option>
                @endforeach
            </select>
        </div>
        <input type="submit" class="btn btn-success" />
    </form>
@endsection