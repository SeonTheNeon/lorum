@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/forums/subcategory/edit/' . $subcategory->id)}}" method="post">
        <div class="panel panel-default">
            <div class="panel-heading">
                SubCategory Information
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" id="title" name="title" class="form-control" value="{{$subcategory->title}}">
                </div>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" id="description" name="description" class="form-control" value="{{$subcategory   ->description}}">
                </div>
            </div>
            <div class="panel-footer">
                <input type="submit" value="Save Changes" class="btn btn-success">
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Special Permissions
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <td>
                            Role
                        </td>
                        <td>
                            Enabled
                        </td>
                        <td>
                            Read
                        </td>
                        <td>
                            Only see own threads?
                        </td>
                        <td>
                            Create
                        </td>
                        <td>
                            Reply
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($permissions as $specialperm)
                        <tr>
                            <td>
                                {{$specialperm->role->display_name}}
                            </td>
                            <td>
                                <input type="checkbox" name="mat[{{$specialperm->role_id}}][enabled]" {{$specialperm->exists ? 'checked' : ''}}>
                            </td>
                            <td>
                                <input type="checkbox" name="mat[{{$specialperm->role_id}}][read]" {{$specialperm->read ? 'checked' : ''}}>
                            </td>
                            <td>
                                <input type="checkbox" name="mat[{{$specialperm->role_id}}][read_only_own]" {{$specialperm->read_only_own ? 'checked' : ''}}>
                            </td>
                            <td>
                                <input type="checkbox" name="mat[{{$specialperm->role_id}}][create]" {{$specialperm->create ? 'checked' : ''}}>
                            </td>
                            <td>
                                <input type="checkbox" name="mat[{{$specialperm->role_id}}][reply]" {{$specialperm->reply ? 'checked' : ''}}>
                            </td>
                        </tr>
                        <input type="hidden" name="mat[{{$specialperm->role_id}}][here]" value="exists">
                    @endforeach
                </tbody>
            </table>
            <div class="panel-footer">
                <input type="submit" value="Save Changes" class="btn btn-success">
            </div>
        </div>
    </form>
@endsection
