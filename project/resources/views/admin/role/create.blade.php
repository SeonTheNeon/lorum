@extends("admin.layout")

@section("content")
    <div>
        <form action="{{URL::to('/admin/roles/create')}}" method="post">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Miscellanous Information
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" required>
                    </div>
                    <div class="form-group">
                        <label for="display_name">Display Name:</label>
                        <input type="text" class="form-control" id="display_name" name="display_name" required>
                    </div>
                    <div class="form-group">
                        <label for="username_style">Username Style:</label>
                        <input type="text" class="form-control" id="username_style" name="username_style" placeholder="{username}" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description:</label>
                        <input type="text" class="form-control" id="description" name="description" required>
                    </div>
                </div>
                <div class="panel-footer">
                    <input type="submit" class="btn btn-success" value="Create">
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Permissions
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <td>
                                Name:
                            </td>
                            <td>
                                Description:
                            </td>
                            <td>
                                Set?
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($perms as $perm)
                            <tr>
                                <td>
                                    {{$perm->name}}
                                </td>
                                <td>
                                    {{$perm->description}}
                                </td>
                                <td>
                                    <input type="checkbox" name="permission[{{$perm->id}}]">
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="panel-footer">
                    <input type="submit" class="btn btn-success" value="Create">
                </div>
            </div>
        </form>
    </div>
@endsection
