@extends("admin.layout")

@section("content")
    <form action="{{URL::to('/admin/roles')}}" method="post">
        <table class="table table-default">
            <thead>
            <tr>
                <td>#</td>
                <td>Name</td>
                <td>Description</td>
                <td>Order</td>
                <td>Actions</td>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{$role->id}}</td>
                    <td>{{$role->display_name}}</td>
                    <td>{{$role->description}}</td>
                    <td><input type="number" name="order[{{$role->id}}]" id="order[{{$role->id}}]" value="{{$role->order}}" min="0" max="99" style="width: 2.5em;"></td>
                    <td>
                        <a href="{{URL::to('/admin/roles/view/' . $role->id)}}" class="btn btn-success">View</a>
                        <a href="{{URL::to('/admin/roles/delete/' . $role->id)}}" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <a href="{{URL::to('/admin/roles/create')}}" class="btn btn-info">Create Role</a>
        <input type="submit" value="Save Order" class="btn btn-success">
    </form>
@endsection
