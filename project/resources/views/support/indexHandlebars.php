<script id="support-table-bones-template" type="text/x-handlebars-template">
    <div class="row">
        <div class="col-md-9">
            <table class="table" id="support-table">
                <thead>
                <tr>
                    <td>ID</td>
                    <td>Type</td>
                    <td>State</td>
                    <td>Author</td>
                    <td>Date</td>
                </tr>
                </thead>
                <tbody id="support-table-body">

                </tbody>
            </table>
            <div id="support-table-footer">

            </div>
        </div>
        <div class="col-md-3">
            <h4>Types</h4>
            <hr />
            {{#each types}}
                <input type="checkbox" data-filter-type="ticketType" data-filter-id="{{id}}" checked/> - {{name}}<BR />
            {{/each}}
            <hr />
            <h4>State</h4>
            <hr />
            <input type="checkbox" data-filter-type="state" data-filter-id="open" checked/> - Open<BR />
            <input type="checkbox" data-filter-type="state" data-filter-id="closed"/> - Closed<BR />
        </div>
    </div>
    <BR />
</script>

<script id="support-table-body-template" type="text/x-handlebars-template">
    {{#each this}}
        <tr class="ticket-type-{{type.id}}-{{state}}">
            <td><a href="/support/ticket/{{id}}">{{id}}</a></td>
            <td><a href="/support/ticket/{{id}}">{{type.name}}</a></td>
            <td>{{state}}</td>
            <td><a href="/profile/{{author.steamid64}}">{{author.personaname}}</a></td>
            <td>{{created_at}}</td>
        </tr>
    {{/each}}
</script>

<script id="support-table-footer-template" type="text/x-handlebars-template">
    <div class="pull-left" style="width: 20%;">
        <input type="button" class="btn btn-info" value="Previous Page" id="support-previous-page">
    </div>
    <div class="pull-left" style="text-align: center; width: 60%;">
        {{from}} to {{to}} of {{total}} total tickets
    </div>
    <div class="pull-right" style="width: 20%; text-align: right;">
        <input type="button" class="btn btn-info" value="Next Page" id="support-next-page">
    </div>
</script>
