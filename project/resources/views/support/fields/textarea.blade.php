<?php global $parser; ?>
@if($view && $data)
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        <p class="well" id="field-{{$field->type}}-{{$field->id}}">
            {!! nl2br($parser->except('youtube')->parse($data)) !!}
        </p>
    </div>
@else
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        @if($field->description)
            <div class="well well-sm">
                {{$field->description}}
            </div>
        @endif
        <textarea class="form-control" id="field-{{$field->type}}-{{$field->id}}" name="field-{{$field->type}}-{{$field->id}}" required>{{old("field-{$field->type}-{$field->id}")}}</textarea>
    </div>

    <script>
        CKEDITOR.replace("field-{{$field->type}}-{{$field->id}}");
    </script>
@endif
