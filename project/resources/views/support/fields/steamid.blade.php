<?php global $parser; ?>
@if($view && $data)
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        <p class="well" id="field-{{$field->type}}-{{$field->id}}">
            {{DB::table('sourcemod_kraken.player_names')->where('steamid', '=', $data)->orderBy('last_used', 'DESC')->select('name')->get()[0]->name}}
        </p>
    </div>
@else
    <div class="form-group">
        <label for="field-{{$field->type}}-{{$field->id}}">{{$field->name}}</label>
        @if($field->description)
            <div class="well well-sm">
                {{$field->description}}
            </div>
        @endif
        <select type="hidden" class="form-control" id="field-{{$field->type}}-{{$field->id}}" name="field-{{$field->type}}-{{$field->id}}" required></select>
    </div>

    <script>
        $('#field-{{$field->type}}-{{$field->id}}').select2({
            minimumInputLength: 1,
            ajax: {
                url: '{{URL::to('/api/forms/search/steamid')}}',
                type: 'GET',
                dataType: 'json',
                contentType: "application/json",
                data: function (params) {
                    return {
                        query: params.term,
                        page: params.page
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.data,
                        pagination: {
                            more: data.current_page !== data.last_page
                        }
                    };
                },
                cache: true
            }
        });
    </script>
@endif
