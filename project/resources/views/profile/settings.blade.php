@extends("app.layout")

@section("container")
    <div>
        <h2>Connected Accounts</h2>

        <div class="panel panel-default">
            <div class="panel-heading">
                Discord
            </div>
            <div class="panel-content">
                @if($user->discord_link)
                    <span style="display: block;">You successfully connected your Discord Account ({{$user->discord_username}}#{{$user->discord_discriminator}})</span>
                @else
                    <a href="{{URL::to('/link/discord')}}">Connect</a> your Discord account to have a better experience on our forums, Discord and our servers.
                @endif
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                Old Forum
            </div>
            <div class="panel-content">
                @if (session('linkforum-error'))
                    <div class="alert alert-danger">
                        {{ session('linkforum-error') }}
                    </div>
                @endif
                @if($user->old_forum())
                    <form action="{{URL::to('/link/forum')}}" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="username" class="col-sm-2 control-label">E-Mail / Username:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="username" name="username" value="{{old('username')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-sm-2 control-label">Password:</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" name="password" value="{{old('password')}}">
                            </div>
                        </div>
                        <div class="btn-group">
                            <input type="submit" class="btn btn-success" value="Login">
                        </div>
                    </form>
                @else
                    <span>You successfully connected your Forum Account</span>
                @endif
            </div>
        </div>
    </div>
@endsection
