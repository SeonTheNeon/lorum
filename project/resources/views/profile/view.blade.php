@extends("app.layout")

@section("container")
    <div class="row">
        <div class="col-md-3">
            <h3>{!! $user->personaname(false) !!}</h3>
            <img src="{{$user->avatarfull}}" class="img-rounded img-responsive">
            <a href="https://steamcommunity.com/profiles/{{$user->steamid64}}">Steam Profile</a><BR>
            <span>Playtime: {{$user->getKrakenStats("playtime")}} minutes</span><BR>
            @if($user->isOnline())
                <span>Currently playing on <a href="steam://connect/{{$user->lastServer()->ip}}:{{$user->lastServer()->port}}">{{$user->lastServer()->name}}</a></span><BR>
            @else
                <span>Currently not playing</span><BR>
            @endif
            @permission(["general_moderator", "general_admin"], false)
                <a href="{{URL::to('/admin/users/view/' . $user->steamid64)}}">Admin CP User Profile</a><BR/>
            @endpermission
        </div>
        <div class="col-md-9 col-md-offset">
            <!--<div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Favourite Map
                        </div>
                        <div class="panel-body">

                        </div>
                    </div>
                </div>
            </div>-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    Recent Threads
                </div>
                <table class="table table-hover">
                    <tbody>
                        @foreach($user->threads()->orderBy('created_at', 'desc')->take(10)->get() as $thread)
                            <tr>
                                <td style="width: 80%">
                                    <a href="{{URL::to('/forum/view/' . $thread->id)}}">{{ $thread->title }}</a>
                                </td>
                                <td>
                                    {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $thread->created_at)->diffForHumans() }}
                                </td>
                            </tr>
                        @endforeach()
                    </tbody>
                </table>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Recent Posts
                </div>
                <table class="table table-hover">
                    <tbody>
                    @foreach($user->posts()->orderBy('created_at', 'desc')->whereHas('thread', function($query) {$query->whereNull('threads.deleted_at');})->take(10)->get() as $post)
                        <tr>
                            <td style="width: 80%">
                                <a href="{{\URL::to('/forum/view/' . $post->thread->id)}}">{{ $post->thread->title }}</a>
                            </td>
                            <td>
                                {{ \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $post->created_at)->diffForHumans() }}
                            </td>
                        </tr>
                    @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
