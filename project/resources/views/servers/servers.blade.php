@extends("app.layout")

@section("container")
    <div class="transparent-background">
        <table class="table table-hover server-table">
            <thead>
                <tr>
                    <th class="text-center">Status</th>
                    <th class="text-center">Name</th>
                    <th class="text-center">Map</th>
                    <th class="text-center">Players</th>
                    <th class="text-center">IP:Port</th>
                </tr>
            </thead>
            <tbody>
                @foreach($server_groups as $server_group)
                    <tr>
                        <td colspan="5" style="font-size: 1.2em;">{{$server_group->name}}</td>
                    </tr>
                    @foreach($server_group->servers()->orderBy("order", "asc")->get() as $i => $server)
                        <tr id="server_{{$server->ip}}_{{$server->port}}" data-ip="{{$server->ip}}" data-port="{{$server->port}}" data-server-id="{{$server->id}}" data-servergroup-id="{{$server_group->id}}" data-iter="{{$i}}" data-seperator="0" @if($i === 0) data-first="1" @else data-first="0" @endif>
                            <td>Pending...</td>
                            <td>Pending...</td>
                            <td>Pending...</td>
                            <td>Pending...</td>
                            <td><a href="steam://connect/{{$server->ip}}:{{$server->port}}">{{$server->ip}}:{{$server->port}}</a></td>
                        </tr>
                        @if($i === 2 && $server_group->servers()->count() > 3 && $server_group->spoilers)
                            <tr id="server_group_{{$server_group->id}}_seperator" data-servergroup-id="{{$server_group->id}}" data-seperator="1">
                                <td colspan="5" align="center">load more servers . . .</td>
                            </tr>
                        @endif
                    @endforeach
                @endforeach
            </tbody>
        </table>
    </div>

    <script>
        jQuery(function() {
            var servers = $("[id^=server_]");
            servers.each(function(i, e) {
                e = $(e);
                if(e.data().seperator === 1) {
                    e.on("click", function(e) {
                        e = e.currentTarget;

                        while(e.nodeName !== "TR")
                            e = e.parentElement;

                        var target = $(e);
                        var serverGroupID = target.data().servergroupId;

                        console.log(target);

                        $("[data-servergroup-id=" + serverGroupID + "]").each(function(item, element) {
                            element = $(element);
                            element.css({"display": ""});
                        });

                        target.css("display", "none");
                    });
                } else {
                    if(e.data().first === 0) {
                        console.log(e.data());
                        if($("[data-servergroup-id=" + e.data().servergroupId + "][data-seperator=1]").length === 1 && e.data().iter > 2) {
                            e.css("display", "none");
                        }
                    }
                    $.get("{{URL::to('/servers/query')}}", {ip: e.data().ip, port: e.data().port}, function(data) {
                        var c = 0;
                        e.children().each(function(i, e) {
                            if(data.online && data.info !== false)
                                switch(c) {
                                    case 0:
                                        var text = "Online";
                                        break;
                                    case 1:
                                        var text = data.info.HostName;
                                        break;
                                    case 2:
                                        var text = data.info.Map;
                                        break;
                                    case 3:
                                        var text = data.info.Players + " / " + data.info.MaxPlayers;
                                        break;
                                }
                            else
                                var text = "Offline";
                            c++;
                            if(c === 1)
                                if(text === "Online")
                                    $(e).css('color', 'green');
                                else
                                    $(e).css('color', 'red');
                            if(c < 5)
                                $(e).text(text);
                        });
                    });
                }
            });
        });
    </script>
@endsection