<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {

    $steamid = '765'.random_int(10000000000000, 99999999999999);
    return [
        'steamid64' => $steamid,
        'personaname' => $faker->name,
        'profileurl' => 'http://steamcommunity.com/profiles/' . $steamid,
        "avatar" => "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/ff/ff3e9b055bf486d7b3056747f705a60d687e9f15.jpg",
        "avatarmedium" => "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/ff/ff3e9b055bf486d7b3056747f705a60d687e9f15_medium.jpg",
        "avatarfull" => "https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/ff/ff3e9b055bf486d7b3056747f705a60d687e9f15_full.jpg",
        'personastate' => 1,
        'realname' => $faker->firstName . " " . $faker->lastName,
        'primaryclanid' => '103582791440132327',
        'timecreated' => \Carbon\Carbon::createFromTimestampUTC(1342284719),
        'personastate' => 0,
        'loccountrycode' => 'DE',
        'locstatecode' => '07',
        'loccityid' => 12786,
        'profilestate' => 1,
        'communityvisibilitystate' => 3,
    ];
});
