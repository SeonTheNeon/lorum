<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServerListTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("server_groups", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("order")->default(0);
            $table->char("name", 128)->default("");
            $table->boolean("active")->default(false);
            $table->timestamps();
        });

        Schema::create("servers", function(Blueprint $table) {
            $table->increments("id");
            $table->integer("order")->default(0);
            $table->char("name", 128)->default("");
            $table->integer("server_group_id");
            $table->char("ip", 15);
            $table->integer("port");
            $table->boolean("active")->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("server_groups");
        Schema::drop("servers");
    }
}
