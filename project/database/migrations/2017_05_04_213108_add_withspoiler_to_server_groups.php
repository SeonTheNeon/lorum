<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWithspoilerToServerGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("server_groups", function(Blueprint $table) {
            $table->boolean("spoilers")->after("order")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("server_groups", function(Blueprint $table) {
            $table->dropColumn("spoilers");
        });
    }
}
