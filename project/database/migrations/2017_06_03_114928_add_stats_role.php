<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatsRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $banTicketSystem = \App\Permission::create(array(
            'name' => 'ban_ticket_system',
            'display_name' => 'Can not use the ticket / support system!',
            'description' => 'Roles with this permission can no longer use the ticket system',
        ));

        $banTicketRole = \App\Role::create(array(
            'name' => 'banned_ticket',
            'display_name' => 'Banned',
            'description' => 'This user is banned from the forum and using the ticket system.',
            'image' => null,
            'username_style' => '<span style="color: SaddleBrown ;">{username}</span>',
            'order' => 99
        ));

        /** @var \App\Role $banTicketRole */
        $banTicketRole->attachPermission($banTicketSystem);

        /** @var \App\Role $bannedRole */
        $bannedRole = \App\Role::whereName('banned')->first();

        foreach($bannedRole->perms as $perm) {
            $banTicketRole->attachPermission($perm);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \App\Permission::whereName('ban_ticket_system')->delete();
        \App\Role::whereName('banned_ticket')->delete();
    }
}
