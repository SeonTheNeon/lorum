<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/forms/render/{ticketType}', 'SupportController@ajaxRenderForm');
Route::get('/forms/search/steamid', 'SupportController@ajaxSearchSteamName');
Route::get('/forms/search/serverid', 'SupportController@ajaxSearchServerName');
