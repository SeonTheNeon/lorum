<?php

/* Random CSS */
Route::get('/css/random.css', 'HelpController@randomCss');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Stuff for logged in Users

Route::get('/link/discord', 'AuthController@loginDiscord')->middleware('throttle:30,1');
Route::post('/link/forum', 'AuthController@linkForum')->middleware('throttle:30,1');
Route::get('/profile/settings', 'ProfileController@showSettings');

// Normal Forum View stuff

Route::get('/forum', 'ForumController@index');

Route::get('/forum/subcat/{subcategory}/new', 'ForumController@newThread');
Route::post('/forum/subcat/{subcategory}/new', 'ForumController@postThread');

Route::get('/forum/subcat/{subcategory}', 'ForumController@viewSubCategory');

Route::get('/forum/view/{thread}', 'ForumController@viewThread');

Route::post('/forum/post/reply', 'ForumController@postNewReply');
Route::post('/forum/post/{post}/edit', 'ForumController@saveChangesToPost');

// Normal Join Us stuff

Route::get('/join', 'JoinController@index');

Route::get('/join/now', 'JoinController@now');

Route::get('/rules/server', 'HelpController@serverRules');

// Logged In
Route::group(['middleware' => ['auth']], function() {
    Route::post('/join/now', 'JoinController@postApplication');
    Route::get('/join/application/view/{application}', 'JoinController@viewApplication');
    Route::post('/join/application/reply/{application}', 'JoinController@replyApplication');
    Route::get('/join/application/vote/{application}', 'JoinController@voteApplication');
    Route::get('/join/list', 'JoinController@listApplications');

    Route::group(['prefix' => 'support'], function() {
        Route::get('/', 'SupportController@index');

        Route::get('/ticket/new', 'SupportController@createTicket');
        Route::post('/ticket/new', 'SupportController@createTicketSave');

        Route::get('/ticket/{ticket}', 'SupportController@viewTicket');
        Route::post('/ticket/{ticket}', 'SupportController@replyTicket');

        Route::get('/list', 'SupportController@ajaxListTickets');
        Route::get('/ticketTypes', 'SupportController@ajaxTicketTypes');
    });
});

// Admin / Moderator Stuff
Route::group(['prefix' => 'admin', 'middleware' => ['permission:general_admin|general_moderator']], function() {
    Route::get('/', 'AdminController@index');

    Route::get('/users', 'AdminController@userIndex');
    Route::get('/users/infinite', 'AdminController@userInfiniteLoading');
    Route::get('/users/typeahead', 'AdminController@userTypeAhead');
    Route::get('/users/view/{user}', 'AdminController@userDetail');
    Route::get('/users/edit/{user}', 'AdminController@userEdit');

    Route::get('/roles', 'AdminController@roleIndex');
    Route::get('/roles/view/{role}', 'AdminController@viewRole');

    Route::get('/forums', 'AdminController@forumIndex');

    Route::get('/forums/category/edit/{category}', 'AdminController@viewCategory');
    Route::get('/forums/subcategory/edit/{subcategory}', 'AdminController@viewSubCategory');

    Route::get('/forum/post/{thread}/delete', 'AdminController@deleteThread');
    Route::get('/forum/post/{thread}/move', 'AdminController@moveThread');

    Route::post('/users/view/{user}/ban', 'AdminController@banUser');

    // Admin only stuff
    Route::group(['middleware' => ['permission:general_admin']], function() {
        Route::post('/users/edit/{user}', 'AdminController@userEditSave');

        Route::post('/roles/view/{role}', 'AdminController@viewRoleSave');
        Route::get('/roles/delete/{role}', 'AdminController@deleteRole');

        Route::get('/roles/create', 'AdminController@createRole');
        Route::post('/roles/create', 'AdminController@createRoleSave');

        Route::post('/roles', 'AdminController@roleIndexSave');

        Route::get('/forums/category/create', 'AdminController@createCategory');
        Route::post('/forums/category/create', 'AdminController@createCategorySave');

        Route::get('/forums/subcategory/create/{category}', 'AdminController@createSubCategory');
        Route::post('/forums/subcategory/create/{category}', 'AdminController@createSubCategorySave');
        Route::get('/forums/subcategory/delete/{subcategory}', 'AdminController@deleteSubCategory');

        Route::get('/forums/category/delete/{category}', 'AdminController@deleteCategory');

        Route::post('/forums', 'AdminController@updateCategoryOrder');

        Route::post('/forums/category/edit/{category}', 'AdminController@viewCategorySave');
        Route::post('/forums/subcategory/edit/{subcategory}', 'AdminController@viewSubCategorySave');

        /* Server List */

        Route::get('/server-list', 'AdminController@serverListIndex');
        Route::post('/server-list', 'AdminController@serverListIndexSave');

        Route::get('/server-list/server/{server}', 'AdminController@serverListEditServer');
        Route::post('/server-list/server/{server}', 'AdminController@serverListEditServerSave');

        Route::get('/server-list/server/{server}/delete', 'AdminController@serverListDeleteServer');

        Route::get('/server-list/server', 'AdminController@serverListNewServer');
        Route::post('/server-list/server', 'AdminController@serverListNewServerSave');

        Route::get('/server-list/servergroup/{servergroup}', 'AdminController@serverListEditServerGroup');
        Route::post('/server-list/servergroup/{servergroup}', 'AdminController@serverListEditServerGroupSave');

        Route::get('/server-list/servergroup/{serverGroup}/delete', 'AdminController@serverListDeleteServerGroup');

        Route::get('/server-list/servergroup', 'AdminController@serverListNewServerGroup');
        Route::post('/server-list/servergroup', 'AdminController@serverListNewServerGroupSave');

        /* Ticket System */

        Route::group(['prefix' => 'ticket'], function() {
            Route::get('/', 'AdminController@ticketIndex');

            Route::get('/type/new', 'AdminController@ticketTypeNew');
            Route::post('/type/new', 'AdminController@ticketTypeCreate');

            Route::get('/type/{ticketType}/edit', 'AdminController@ticketTypeEdit');
            Route::post('/type/{ticketType}/edit', 'AdminController@ticketTypeUpdate');
            Route::get('/type/{ticketType}/delete', 'AdminController@ticketTypeDelete');

            Route::get('/type/{ticketType}/field/new', 'AdminController@ticketTypeFieldNew');
            Route::post('/type/{ticketType}/field/new', 'AdminController@ticketTypeFieldCreate');

            Route::get('/type/{ticketType}/field/{ticketTypeField}/edit', 'AdminController@ticketTypeFieldEdit');
            Route::post('/type/{ticketType}/field/{ticketTypeField}/edit', 'AdminController@ticketTypeFieldUpdate');
            Route::get('/type/{ticketType}/field/{ticketTypeField}/delete', 'AdminController@ticketTypeFieldDelete');
        });
    });
});


Route::get('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::get('/', 'HelpController@index');

Route::get('/profile/{user}', 'ProfileController@showProfile');

// Server Browser
Route::get('/servers', 'ServerController@index');
Route::get('/servers/query', 'ServerController@serverQuery');

// Team List Page
Route::get('/team', 'HelpController@showServerTeam');
