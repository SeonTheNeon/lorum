<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Server;
use App\Models\ServerGroup;
use App\Permission;
use App\Role;
use App\SubCategory;
use App\SubCategoryRolePermission;
use App\Thread;
use App\TicketType;
use App\TicketTypeField;
use App\TicketTypeRole;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class AdminController extends Controller {

    public function __construct() {
        //
    }

    /*
     * DASHBOARD
     */

    public function index() {
        return view('admin.index');
    }

    /*
     * USERS
     */

    public function userIndex() {
        $users = User::take(10)->get();
        return view('admin.user.index', compact('users'));
    }

    public function userInfiniteLoading(Request $request) {
        $page = $request->get('page', 0);
        $word = $request->get('word', '');
        $users = User::where('personaname', 'like', '%' . $word . '%')->skip($page * 10)->take(10)->get();
        return $users;
    }

    public function userTypeAhead(Request $request) {
        $word = $request->get('word');
        if(is_null($word))
            return "";
        $users = User::where('personaname', 'like', '%' . $word . '%')->get()->all();
        return $users;
    }

    public function userDetail(User $user) {
        return view('admin.user.detail', compact('user'));
    }

    public function userEdit(User $user) {

        $roles = Role::all();

        if(strlen($user->loccityid) > 0 && strlen($user->locstatecode > 0 ) && strlen($user->loccountrycode) > 0)
            $location = DB::select(DB::raw("SELECT loc_countries.name AS country_name, loc_states.name AS state_name, loc_cities.name AS city_name FROM loc_countries JOIN loc_states ON loc_states.country_id = loc_countries.id JOIN loc_cities ON loc_cities.state_id = loc_states.id WHERE loc_cities.city_id = " . $user->loccityid . " AND loc_states.state_id = \"" . $user->locstatecode . "\" AND loc_countries.country_id = \"" . $user->loccountrycode . "\""));

        return view('admin.user.edit', compact('user', 'roles', 'location'));
    }

    public function userEditSave(User $user, Request $request) {

        // Edit Role
        $role = Role::findOrFail($request->get('role'));
        $user->detachRoles();
        $user->attachRole($role);

        return redirect('/admin/users/edit/' . $user->steamid64);
    }

    public function banUser(User $user, Request $request) {
        $this->validate($request, [
            'until' => 'required|date|after:now|before:+1 year|date_format:m/d/Y'
        ], [
            'until.required' => 'You have to enter a date before trying to ban someone!',
            'until.after' => 'You can only ban people into the future :D',
            'until.before' => 'You can only ban people for one year, sorry my friend :(',
        ]);

        if($user->roles()->first()->order < Auth::user()->roles()->first()->order) {
            return back()->with('You can not ban an user with a higher rank.');
        } else {
            $date = Carbon::createFromFormat("m/d/Y", $request->get('until'));

            $user->ban($date, Auth::user());

            return back();
        }
    }

    /*
     * ROLES
     */

    public function roleIndex() {
        $roles = Role::orderBy('order', 'ASC')->get();
        return view('admin.role.index', compact("roles"));
    }

    public function roleIndexSave(Request $request) {
        $data = $request->toArray();
        foreach($data['order'] as $id => $order) {
            $role = Role::findOrFail($id);
            $role->order = $order;
            $role->save();
        }
        return back();
    }

    public function viewRole(Role $role) {
        return view('admin.role.view', compact("role"));
    }

    public function viewRoleSave(Role $role, Request $request) {
        $role->fill($request->toArray());

        $role->detachPermissions($role->perms);

        if(!is_null($request->toArray()['permission']))
            foreach($request->toArray()['permission'] as $perm => $state) {
                $role->attachPermission(Permission::findOrFail($perm));
            }

        $role->save();

        return back();
    }

    public function createRole() {
        $perms = Permission::all();
        return view('admin.role.create', compact('perms'));
    }

    public function createRoleSave(Request $request) {
        $role = new Role();
        $role->fill($request->toArray());
        $role->save();

        foreach($request->toArray()['permission'] as $perm => $state) {
            $role->attachPermission(Permission::findOrFail($perm));
        }

        return redirect(URL::to('/admin/roles/view/' . $role->id));
    }

    /*
     * FORUMS
     */

    public function forumIndex() {
        $categories = Category::orderBy('order', 'asc')->get();
        return view('admin.forum.index', compact('categories'));
    }

    public function createCategory() {
        return view('admin.forum.create_category');
    }

    public function createCategorySave(Request $request) {
        $category = new Category();
        $category->fill($request->toArray());
        $category->saveOrFail();

        return redirect(URL::to('/admin/forums'));
    }

    public function createSubCategory(Category $category) {
        $id = $category->id;
        return view('admin.forum.create_subcategory', compact('id'));
    }

    public function createSubCategorySave(Request $request, Category $category) {
        $subcategory = new SubCategory();
        $subcategory->fill($request->toArray());
        $subcategory->category_id = $category->id;
        $subcategory->saveOrFail();

        return redirect(URL::to('/admin/forums'));
    }

    public function deleteCategory(Category $category) {
        $category->delete();
        return back();
    }

    public function updateCategoryOrder(Request $request) {
        foreach($request->toArray()['order']['category'] as $category_id => $order) {
            $category = Category::findOrFail($category_id);
            $category->order = $order;
            $category->saveOrFail();
        }

        return back();
    }

    public function viewCategory(Category $category) {
        return view('admin.forum.view_category', compact('category'));
    }

    public function viewCategorySave(Category $category, Request $request) {
        $category->fill($request->toArray());
        $category->saveOrFail();

        foreach($request->toArray()['subcategory_order'] as $subcategory_id => $order) {
            $subcategory = SubCategory::findOrFail($subcategory_id);
            $subcategory->order = $order;
            $subcategory->saveOrFail();
        }

        return back();
    }

    public function viewSubCategory(SubCategory $subcategory) {
        $specialperms = $subcategory->specialPermissions()->get();
        $permissions = array();

        foreach(Role::all() as $role) {
            $isin = false;
            $inperm = null;

            foreach($specialperms as $perm) {
                if(intval($perm->role_id) === intval($role->id)) {
                    $isin = true;
                    $inperm = $perm;
                    break;
                }
            }

            if(!$isin) {
                $inperm = new SubCategoryRolePermission();
                $inperm->role_id = $role->id;
                $inperm->subcategory_id = $subcategory->id;
            }

            $permissions[] = $inperm;
        }

        return view('admin.forum.view_subcategory', compact('subcategory', 'permissions'));
    }

    public function deleteThread(Thread $thread) {
        $thread->delete();
        return back()->with('status', 'Successfully deleted thread "' . $thread->title . '"!');
    }

    public function moveThread(Thread $thread, Request $request) {
        $subCategory = SubCategory::findOrFail($request->get('subcategory'));
        $thread->subcategory_id = $subCategory->id;
        $thread->save();
        return redirect('/forum/subcat/' . $subCategory->id)->with('status', 'Successfully moved thread "' . $thread->title . '"!');
    }

    public function viewSubCategorySave(SubCategory $subcategory, Request $request) {
        $subcategory->fill($request->toArray());
        $subcategory->saveOrFail();

        foreach($request->toArray()['mat'] as $roleid => $settings) {
            $enabled = array_key_exists("enabled", $settings);
            if($enabled) {
                $scrp = SubCategoryRolePermission::firstOrNew(array(
                    "role_id" => $roleid,
                    "subcategory_id" => $subcategory->id
                ));

                $scrp->read = array_key_exists("read", $settings);
                $scrp->read_only_own = array_key_exists("read_only_own", $settings);
                $scrp->create = array_key_exists("create", $settings);
                $scrp->reply = array_key_exists("reply", $settings);

                $scrp->save();
            } else {
                $scrp = SubCategoryRolePermission::where("role_id", "=", $roleid)->where("subcategory_id", "=", $subcategory->id)->first();
                if(!is_null($scrp))
                    $scrp->delete();
            }
        }

        return back();
    }

    public function deleteRole(Role $role) {
        return $role->delete() ? back() : back()->with('status', 'Could not delete role! :(');
    }

    public function deleteSubCategory(SubCategory $subcategory) {
        $subcategory->delete();
        return back();
    }

    /*
    Server List
    */

    public function serverListIndex() {
        $servergroups = ServerGroup::orderBy("order", "asc")->get();
        return view("admin.server-list.index", compact("servergroups"));
    }

    public function serverListIndexSave(Request $request) {
        foreach($request->get("server_orders") as $server_id => $server_order) {
            $server = Server::whereId($server_id)->first();
            $server->order = $server_order;
            $server->active = ($request->get("server_enabled", array()) && isset($request->get("server_enabled", array())[$server->id])) ? true : false;
            $server->save();
        }

        foreach(ServerGroup::all() as $servergroup) {
            $servergroup->active = ($request->get("servergroup_enabled", array()) && isset($request->get("servergroup_enabled", array())[$servergroup->id])) ? true : false;
            $servergroup->order = $request->get("servergroup_order")[$servergroup->id];
            $servergroup->save();
        }

        return redirect("/admin/server-list");
    }

    public function serverListEditServer(Request $request, Server $server) {
        $servergroups = ServerGroup::all();
        return view("admin.server-list.server.edit", compact("servergroups", "server"));
    }

    public function serverListEditServerSave(Request $request, Server $server) {
        $server->fill($request->toArray());
        $server->save();
        return redirect("/admin/server-list");
    }

    public function serverListNewServer(Request $request) {
        $servergroups = ServerGroup::all();
        return view("admin.server-list.server.new", compact("servergroups"));
    }

    public function serverListNewServerSave(Request $request) {
        Server::create($request->toArray());
        return redirect("/admin/server-list");
    }

    public function serverListDeleteServer(Server $server) {
        $server->delete();
        return back();
    }

    public function serverListEditServerGroup(Request $request, ServerGroup $servergroup) {
        return view("admin.server-list.servergroup.edit", compact("servergroup"));
    }

    public function serverListEditServerGroupSave(Request $request, ServerGroup $servergroup) {
        $servergroup->fill($request->toArray());
        $servergroup->spoilers = ($request->get("spoilers") !== null) ? true : false;
        $servergroup->save();
        return redirect("/admin/server-list");
    }

    public function serverListNewServerGroup(Request $request) {
        return view("admin.server-list.servergroup.new");
    }

    public function serverListNewServerGroupSave(Request $request) {
        ServerGroup::create($request->toArray());
        return redirect("/admin/server-list");
    }

    public function serverListDeleteServerGroup(ServerGroup $serverGroup) {
        $serverGroup->delete();
        return back();
    }

    /* TICKET SYSTEM */

    public function ticketIndex() {
        $ticketTypes = TicketType::all();

        return view('admin.ticket.index', compact('ticketTypes'));
    }

    public function ticketTypeNew(){
        return view('admin.ticket.type.new');
    }

    public function ticketTypeCreate(Request $request) {
        $ticketType = TicketType::create($request->toArray());
        if($ticketType->id) {
            return redirect(URL::to('/admin/ticket/type/' . $ticketType->id . '/edit'));
        } else {
            return back()->with('status', 'Something bad happened during the creation of your ticket type :( Please contact @Zelenka#4001 on Discord');
        }
    }

    public function ticketTypeEdit(TicketType $ticketType) {
        $roles = Role::orderBy('order', 'asc')->get();
        $fields = $ticketType->fields()->get();
        return view('admin.ticket.type.edit', compact('ticketType', 'roles', 'fields'));
    }

    public function ticketTypeUpdate(TicketType $ticketType, Request $request) {
        $ticketType->fill($request->except('role'));
        $ticketType->save();

        $ticketTypeRoles = $ticketType->roles()->get();

        foreach($request->get("role", array()) as $role => $state) {
            $found = false;

            foreach($ticketTypeRoles as $ticketTypeRole) {
                if($role === $ticketTypeRole->id) {
                    $ticketTypeRole->found = true;
                    $found = true;
                }
            }

            if(!$found) {
                $ticketTypeRole = new TicketTypeRole();
                $ticketTypeRole->ticket_type_id = $ticketType->id;
                $ticketTypeRole->role_id = $role;
                $ticketTypeRole->saveOrFail();
            }
        }

        foreach($ticketTypeRoles as $ticketTypeRole) {
            if(!$ticketTypeRole->found)
                $ticketTypeRole->delete();
        }

        return back()->with('status', 'Update successful!');
    }

    public function ticketTypeDelete(TicketType $ticketType) {
        $ticketType->delete();
        return back()->with('status', "Successfully deleted TicketType '{$ticketType->name}'!");
    }

    public function ticketTypeFieldNew(TicketType $ticketType) {
        return view('admin.ticket.field.new', compact('ticketType'));
    }

    public function ticketTypeFieldCreate(TicketType $ticketType, Request $request) {
        $ticketTypeField = new TicketTypeField();
        $ticketTypeField->fill($request->toArray());
        $ticketTypeField->ticket_type_id = $ticketType->id;
        $ticketTypeField->saveOrFail();
        return redirect(URL::to("/admin/ticket/type/{$ticketType->id}/field/{$ticketTypeField->id}/edit"));
    }

    public function ticketTypeFieldEdit(TicketType $ticketType, TicketTypeField $ticketTypeField) {
        return view('admin.ticket.field.edit', compact('ticketType', 'ticketTypeField'));
    }

    public function ticketTypeFieldUpdate(TicketType $ticketType, TicketTypeField $ticketTypeField, Request $request) {
        $ticketTypeField->fill($request->toArray());
        $ticketTypeField->saveOrFail();
        return redirect(URL::to('/admin/ticket/type/' . $ticketType->id . '/edit'))->with('status', "Successfully updated the TicketTypeField '{$ticketTypeField->name}'!");
    }

    public function ticketTypeFieldDelete(TicketType $ticketType, TicketTypeField $ticketTypeField, Request $request) {
        $ticketTypeField->delete();
        return redirect(URL::to('/admin/ticket/type/' . $ticketType->id . '/edit'))->with('status', "Successfully deleted the TicketTypeField '{$ticketTypeField->name}'!");
    }
}
