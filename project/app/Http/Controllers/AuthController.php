<?php

namespace App\Http\Controllers;

use App\OldUser;
use Carbon\Carbon;
use Discord\OAuth\Discord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Invisnik\LaravelSteamAuth\SteamAuth;
use App\User;
use App\Role;

class AuthController extends Controller
{
    /**
     * @var SteamAuth
     */
    private $steam;

    public function __construct(SteamAuth $steam) {
        $this->steam = $steam;
    }

    public function logout() {
        Auth::logout();
        return back();
    }

    public function login() {
        if ($this->steam->validate()) {
            $info = $this->steam->getUserInfo();
            if (!is_null($info)) {
                $user = User::where('steamid64', $info->steamID64)->first();
                if (is_null($user)) {
                    $user = User::create([
                        'steamid64'  => $info->steamID64
                    ]);

                    $userrole = Role::where('name', '=', 'user')->first(); //TODO: fix hardcoded normal user role
                    $user->attachRole($userrole);
                    
                    $user->save();
                }

                $user->fill($info->toArray());

                if(strlen($user->lastlogoff) > 3)
                    $user->lastlogoff = Carbon::createFromTimestampUTC($user->lastlogoff);
                if(strlen($user->timecreated) > 3)
                    $user->timecreated = Carbon::createFromTimestampUTC($user->timecreated);

                $user->save();

                Auth::login($user, true);
                return redirect(URL::to('/')); // redirect to site
            }
        }
        return $this->steam->redirect(); // redirect to Steam login page
    }

    public function loginDiscord() {

        if(Auth::guest())
            return redirect("/");

        $provider = new Discord([
            'clientId' => env('DISCORD_ID'),
            'clientSecret' => env('DISCORD_SECRET'),
            'redirectUri' => env('APP_URL') . 'link/discord'
        ]);

        if(!isset($_GET['code'])) {
            return redirect($provider->getAuthorizationUrl());
        } else {

            $token = $provider->getAccessToken('authorization_code', [
                'code' => $_GET['code'],
            ]);

            $user = Auth::user();

            // Get the user object.
            $discuser = $provider->getResourceOwner($token);

            $user->discord_id = $discuser->id;
            $user->discord_username = $discuser->username;
            $user->discord_email = $discuser->email;
            $user->discord_discriminator = $discuser->discriminator;
            $user->discord_verified = $discuser->verified;
            $user->discord_mfa_enabled = $discuser->mfa_enabled;
            $user->discord_token = $token->getRefreshToken();
            $user->discord_link = true;
            $user->save();
        }

        return redirect('/profile/settings');
    }

    public function linkForum(Request $request) {

        if(Auth::guest())
            return redirect("/");

        $usermail = $request->get('username');
        $pass = $request->get('password');
        $newuser = Auth::user();

        if(OldUser::where('username', '=', $usermail)->orWhere('email', '=', $usermail)->count() === 1 &&
            $user = OldUser::where('username', '=', $usermail)->orWhere('email', '=', $usermail)->first()) {
            if($user->verifyPassword($pass)) {
                DB::table("posts")
                    ->where('user_id', '=', $user->id)
                    ->update(['user_id' => $newuser->steamid64]);
                DB::table("threads")
                    ->where('user_id', '=', $user->id)
                    ->update(['user_id' => $newuser->steamid64]);
                $user->linked = true;
                $user->steamid64 = $newuser->steamid64;
                $user->save();
                return back();
            }
        }
        return redirect('/profile/settings')->with('linkforum-error', 'Incorrect Username / Password')->withInput($request->toArray());
    }
}
