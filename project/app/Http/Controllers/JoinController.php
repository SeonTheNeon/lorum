<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Application;
use App\Models\ApplicationResponse;
use App\Models\Vote;
use App\Post;
use App\Role;
use App\SubCategory;
use App\Thread;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class JoinController extends Controller {

    public function index() {
        if(!Auth::guest()) {
            if(Auth::user()->can('general_admin') || Auth::user()->can('join_admin')) {
                return redirect('/join/list');
            } elseif(Auth::user()->applications()->count() > 0) {
                return redirect('/join/application/view/' . Auth::user()->applications()->first()->id);
            }
        }
        return view('join.index');
    }

    public function now() {
        return view('join.now');
    }

    public function postApplication(Request $request) {
        if(Application::where('author_id', '=', Auth::user()->steamid64)->count() > 0)
            return redirect('/join/application/view/' . Application::where('author_id', '=', Auth::user()->steamid64)->first()->id)->with('status', 'You already posted an application');

        $application = new Application();
        $application->fill($request->toArray());
        $application->author_id = Auth::user()->steamid64;
        $application->birthday = Carbon::createFromFormat('m/d/Y', $application->birthday);
        $application->save();

        $admin_users = User::getUsersWithPermission("join_admin");

        foreach($admin_users as $admin_user) {
            $admin_user->sendDiscordMessage("Hey " . (($admin_user->realname) ? $admin_user->realname : $admin_user->personaname) . "! " . Auth::user()->personaname ." has written a new application! View it on " . URL::to('/join/application/view/' . $application->id));
        }

        return redirect('/join/application/view/' . $application->id);
    }

    public function viewApplication(Application $application) {
        if($application->author_id === Auth::user()->steamid64 || Auth::user()->can('join_admin') || Auth::user()->can('general_admin')) {
            foreach($application->responses as $response) {
                $response->read(Auth::user());
            }
            return view('join.view', compact("application"));
        }
        else
            return back()->with('status', 'Permission Denied');
    }

    public function replyApplication(Application $application, Request $request) {
        if(!($application->author_id === Auth::user()->steamid64 || Auth::user()->can('join_admin') || Auth::user()->can('general_admin')))
            return back()->with('status', 'Permission Denied');

        $answer = new ApplicationResponse();
        $answer->message = $request->get('text');
        $answer->author_id = Auth::user()->steamid64;
        if(Auth::user()->can('join_admin'))
            $answer->type = $request->get('action', 'regular');
        else
            $answer->type = "regular";
        $answer->application_id = $application->id;
        $answer->save();

        /** @var User $user */
        $user = $application->author()->first();

        if(Auth::user()->can('join_admin'))
            switch($answer->type) {
                case "accept":
                    $application->state = "accepted";
                    $role = Role::where('name', '=', 'moderator')->first();
                    $user->detachRoles();
                    $user->attachRole($role);
                    $user->sendDiscordMessage("Welcome to our team, " . $user->personaname . "!\n\rView your application at " . URL::to('/join/application/view/' . $application->id));
                    break;
                case "reject":
                    $application->state = "rejected";
                    $user->sendDiscordMessage("Your application has been denied! Sorry " . $user->personaname . "!\n\rView your application at " . URL::to('/join/application/view/' . $application->id));
                    break;
                case "reopen":
                    $application->state = "reopened";
                    $user->sendDiscordMessage("Your application has been reopened! Good Luck " . $user->personaname . "!\n\rView your application at " . URL::to('/join/application/view/' . $application->id));
                    break;
                case "regular":
                    $user->sendDiscordMessage("You have received a new message from  " . Auth::user()->personaname. "!\n\rView your application at " . URL::to('/join/application/view/' . $application->id));
            }

        $application->save();

        return back();
    }

    public function voteApplication(Application $application, Request $request) {
        if(!(Auth::user()->can('join_admin') || Auth::user()->can('general_admin')))
            return back()->with('status', 'Permission Denied');

        if($application->state === "opened" || $application->state === "reopened") {
            Vote::updateOrCreate([
                'user_id' => Auth::user()->steamid64,
                'application_id' => $application->id,
            ], [
                'for' => $request->get('what'),
            ]);
            return back()->with('status', 'You successfully voted for "' . $request->get('what') . '"');
        } else {
            return back()->with('status', 'You can not vote on an application which is already closed.');
        }
    }

    public function listApplications() {
        if(!(Auth::user()->can('join_admin') || Auth::user()->can('general_admin')))
            return back()->with('status', 'Permission Denied');

        $applications = Application::orderBy('created_at', 'desc')->paginate();

        return view('join.list', compact("applications"));
    }
}
