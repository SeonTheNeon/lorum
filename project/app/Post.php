<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Post extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = "posts";

    protected $fillable = [
        'content'
    ];

    public function thread() {
        return $this->hasOne('App\Thread', 'id', 'thread_id');
    }

    public function author() {
        if(strlen($this->user_id) < 17)
            return $this->hasOne('App\OldUser', 'id', 'user_id');
        else
            return $this->hasOne('App\User', 'steamid64', 'user_id');
    }

    public function editor() {
        return $this->hasOne('App\User', 'steamid64', 'editor_id');
    }

    public function isEditable() {
        if(Auth::guest()) {
            return false;
        } else {
            /** @var User $user */
            $user = Auth::user();

            if($user->steamid64 === $this->user_id) {
                return $user->roles()->first()->hasPermission('post_edit_own');
            } else {
                $allowed = $user->can('general_admin') || $user->can('general_moderator');
                if($allowed) {
                    if($this->author->isFake())
                        return true;
                    else
                        return $this->author->roles()->first()->order >= Auth::user()->roles()->first()->order;
                } else {
                    return false;
                }
            }
        }

        return false;
    }
}
