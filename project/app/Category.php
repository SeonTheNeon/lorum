<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = "categories";

    protected $fillable = [
        'title', 'description', 'order'
    ];

    public function subcategories() {
        return $this->hasMany('App\SubCategory', 'category_id', 'id')->orderBy('order', 'asc');
    }

    public function hasAccess() {
        $i = 0;
        $noaccess = 0;

        foreach($this->subcategories as $subcategory) {
            $i++;
            if(!$subcategory->hasAccess())
                $noaccess++;
        }

        return $i !== $noaccess;
    }
}
