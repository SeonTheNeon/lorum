<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model {

    protected $table = "application_votes";

    protected $fillable = ['for', 'user_id', 'application_id'];

    public function application() {
        return $this->hasMany('App\Models\ApplicationResponse');
    }

    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function allMessagesRead($user = null) {
        if(is_null($user))
            if(Auth::guest()) {
                return true;
            } else {
                $user = Auth::user();
            }

        $allmessages = $this->responses()->all();
        foreach($allmessages as $message) {
            if(!$message->read($user))
                return false;
        }
        return true;
    }

}
