<?php

namespace App\Providers;

use App\Models\Application;
use Golonka\BBCode\BBCodeParser;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        global $parser;
        $parser = new BBCodeParser();
        $parser->setParser('size', '/\[size\=([0-9]{1,3})\](.*?)\[\/size\]/s', '<span style="font-size: $1%;">$2</span>', '$2');
        $parser->setParser('image', '/\[img\](.*?)\[\/img\]/s', '<img src="$1" class="img-responsive">', '$i');
        $parser->setParser('hr', '/\[hr\]/s', '<hr>', '$i');
        $parser->setParser('align', '/\[align\=(center|left|right)\](.*?)\[\/align\]/s', '<span style="text-align: $1;">$2</span>', '$2');
        $parser->setParser('color-alt', '/\[color\=([#]?[a-zA-Z0-9]+)\](.*?)\[\/color\]/s', '<span style="color: $1;">$2</span>', '$2');

        Blade::directive('random_meme', function ($expression) {
            $base_url = URL::to('/');
            return '<?php echo "' . $base_url . '/img/3kliks_memes/" . collect(scandir(public_path(\'/img/3kliks_memes\')))->splice(2)->random(); ?>';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
