<?php

namespace App;

use App\Events\UserCreated;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $primaryKey = "steamid64";
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'personaname', 'profileurl', 'avatar',
        'avatarmedium', 'avatarfull', 'personastate',
        'communityvisibilitystate', 'profilestate',
        'lastlogoff', 'commentpermission', 'realname',
        'primaryclanid', 'timecreated', 'gameid',
        'gameserverip', 'gameextrainfo', 'loccountrycode',
        'locstatecode', 'loccityid', 'steamid64'
    ];

    protected $visible = [
        'personaname', 'profileurl', 'avatar',
        'avatarmedium', 'avatarfull', 'personastate',
        'communityvisibilitystate', 'profilestate',
        'lastlogoff', 'commentpermission', 'realname',
        'primaryclanid', 'timecreated', 'gameid',
        'gameserverip', 'gameextrainfo', 'loccountrycode',
        'locstatecode', 'loccityid', 'steamid64',
    ];

    protected $events = [
        'created' => UserCreated::class,
    ];

    protected $hidden = [
        'remember_token',
    ];


    public static function findAlt($x) {
        $rslt = static::find($x);
        if(is_null($rslt)) {
            $rslt = new User();
            $rslt->steamid64 = $x;
            $rslt->personaname = htmlspecialchars(DB::table('sourcemod_kraken.player_names')->where('steamid', '=', $x)->orderBy('last_used', 'DESC')->select('name')->get()[0]->name);
        }
        return $rslt;
    }

    public function threads() {
        return $this->hasMany('App\Thread', 'user_id', 'steamid64');
    }

    public function posts() {
        return $this->hasMany('App\Post', 'user_id', 'steamid64');
    }

    public function banned() {
        return !Carbon::createFromFormat("Y-m-d H:i:s", $this->banned_until)->isPast();
    }

    public function applications() {
        return $this->hasMany('App\Models\Application', 'author_id', 'steamid64');
    }

    public function getLocation() {
        if($this->loccountrycode) {
            $lcc = LocCountry::where("country_id", $this->loccountrycode)->first();
            if($this->locstatecode) {
                $lsc = LocState::where("state_id", $this->locstatecode)->where('country_id', $lcc->id)->first();
                if($this->loccityid) {
                    $lci = LocCity::where("city_id", $this->loccityid)->where("state_id", $lsc->id)->first();
                    return $lci->name . ", " . $lsc->name . ", " . $lcc->name;
                } else {
                    return $lsc->name . ", " . $lcc->name;
                }
            } else {
                return $lcc->name;
            }
        } else {
            return "Unknown Location";
        }
    }

    public function ban(Carbon $until, User $judge) {
        $this->banned_until = $until;
        $this->detachRole($this->roles->first());
        $this->attachRole(Role::where('name', '=', 'banned')->first());

        $entry = new BanHistory();
        $entry->judge_id = $judge->steamid64;
        $entry->victim_id = $this->steamid64;
        $entry->from = Carbon::now();
        $entry->to = $until;

        return $this->save();
    }

    public function unban() {
        $this->banned_until = Carbon::now();
        $this->detachRole($this->roles->first());
        $this->attachRole(Role::where('name', '=', 'user')->first());
        return $this->save();
    }

    public function personaname($link = true) {
        if($this->roles()->count() > 0)
            return str_replace("{username}", htmlspecialchars($this->personaname), ($link ? "<a href='" . URL::to('/profile/' . $this->steamid64) . "'>" : "") . $this->roles()->first()->username_style . ($link ? '</a>' : ''));
        else
            return $this->personaname;
    }

    private $kraken;

    public function getKrakenStats($type = "playtime") {
        if(!Cache::has('player-kraken-' . $type . '-' . $this->steamid64)) {
            $result = DB::select(DB::raw("SELECT sum(value) AS aggregate FROM sourcemod_kraken.player_stats JOIN sourcemod_kraken.players ON players.uid = player_stats.uid WHERE players.steamid = '" . $this->steamid64 . '\' AND type = \'' . $type . '\' GROUP BY player_stats.type '));

            if(count($result) > 0)
                Cache::put('player-kraken-' . $type . '-' . $this->steamid64, $result[0]->aggregate, 5);
            else
                Cache::put('player-kraken-' . $type . '-' . $this->steamid64, 0, 5);
        }

        return Cache::get('player-kraken-' . $type . '-' . $this->steamid64);
    }

    public function getKrakenUID() {
        return Cache::remember('user-krakenuid-' . $this->steamid64, 60*24, function () {
            return DB::table('sourcemod_kraken.players')->where('steamid', '=', $this->steamid64)->select('uid')->first()->uid;
        });
    }

    /**
     * Returns if the player is currently playing on our servers
     * @return boolean
     */
    public function isOnline() {
        if(!Cache::has('player-isonline-' . $this->steamid64)) {
            $last = DB::select(DB::raw("SELECT start, end, ip FROM sourcemod_kraken.player_session JOIN sourcemod_kraken.players ON players.uid = player_session.uid WHERE players.steamid = '" . $this->steamid64 . '\' ORDER BY start DESC LIMIT 1'));
            if(count($last) === 0)
                Cache::put('player-isonline-' . $this->steamid64, false, 5);
            else
                Cache::put('player-isonline-' . $this->steamid64, $last[0]->start === $last[0]->end, 5);;
        }

        return Cache::get('player-isonline-' . $this->steamid64);
    }

    /**
     * Returns the last server the player was on
     * @return boolean|array
     */
    public function lastServer() {
        if(!Cache::has('player-lastserver-' . $this->steamid64)) {
            $last = DB::select(DB::raw("SELECT servers.ip AS ip, servers.port AS port, servers.name AS name FROM sourcemod_kraken.player_session JOIN sourcemod_kraken.players ON players.uid = player_session.uid JOIN sourcemod_kraken.servers ON servers.sid = player_session.sid WHERE players.steamid = '" . $this->steamid64 . '\' ORDER BY start DESC LIMIT 1'));
            if(count($last) === 0)
                Cache::put('player-lastserver-' . $this->steamid64, false, 5);
            else
                Cache::put('player-lastserver-' . $this->steamid64, $last[0], 5);
        }

        return Cache::get('player-lastserver-' . $this->steamid64);
    }

    public function retakeElo($default = 1000) {
        if(!Cache::has('player-elo-' . $this->steamid64)) {
            $elo = DB::select(DB::raw('SELECT elo FROM sourcemod_kraken.player_elo JOIN sourcemod_kraken.players ON player_elo.uid = players.uid WHERE players.steamid = "' . $this->steamid64 . '"'));
            if(count($elo) === 1) {
                Cache::put('player-elo-' . $this->steamid64, $elo[0]->elo, 5);
            } else {
                Cache::put('player-elo-' . $this->steamid64, $default, 5);
            }
        }

        return Cache::get('player-elo-' . $this->steamid64);
    }

    public function getRankByElo() {
        $elo = $this->retakeElo();
    }

    public function old_forum() {
        $olduser = OldUser::where('steamid64', '=', $this->steamid64)->where('linked', '=', true)->first();
        if(is_null($olduser))
            return true;
        else
            return false;
    }

    public function isFake() {
        return false;
    }

    public static function getUsersWithPermission($permission) {
        $admin_roles = Role::whereHas('perms', function($query) use ($permission) {
            $query->where('name', '=', $permission);
        })->get();

        $admin_users = User::whereHas('roles', function($query) use ($admin_roles) {
            $ids = array();
            foreach($admin_roles as $admin_role) {
                $ids[] = $admin_role->id;
            }

            $query->whereIn("id", $ids);
        })->get();

        return $admin_users;
    }

    public static function getUsersWithRoleIDs($roleIDs) {
        $admin_roles = Role::whereIn('id', $roleIDs)->get();

        $admin_users = User::whereHas('roles', function($query) use ($admin_roles) {
            $ids = array();
            foreach($admin_roles as $admin_role) {
                $ids[] = $admin_role->id;
            }

            $query->whereIn("id", $ids);
        })->get();

        return $admin_users;
    }

    public function sendDiscordMessage($message) {
        if($this->discord_id)
            DB::insert('INSERT INTO discord.msg_queue (user_id, message) VALUES (?, ?)', [$this->discord_id, $message]);
    }

    // https://gist.github.com/rannmann/49c1321b3239e00f442c#file-steamuserfunctions-php-L41-L51
    public function steamid2($short = false) {
        $id = $this->steamid64;
        if (is_numeric($id) && strlen($id) >= 16) {
            $z = bcdiv(bcsub($id, '76561197960265728'), '2');
        } elseif (is_numeric($id)) {
            $z = bcdiv($id, '2'); // Actually new User ID format
        } else {
            return $id; // We have no idea what this is, so just return it.
        }
        $y = bcmod($id, '2');
        if($short) {
            return floor($z);
        } else {
            return 'STEAM_0:' . $y . ':' . floor($z);
        }
    }

    // https://gist.github.com/rannmann/49c1321b3239e00f442c#file-steamuserfunctions-php-L52-L62
    public function steamid3() {
        $id = $this->steamid64;
        if (preg_match('/^STEAM_/', $id)) {
            $split = explode(':', $id);
            return "[U:1:" . $split[2] * 2 + $split[1] . "]";
        } elseif (preg_match('/^765/', $id) && strlen($id) > 15) {
            return "[U:1:" . bcsub($id, '76561197960265728') . "]";
        } else {
            return $id; // We have no idea what this is, so just return it.
        }
    }

    public function steamid64() {
        return $this->steamid64;
    }
}
