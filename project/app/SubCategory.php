<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class SubCategory extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $table = "subcategories";

    protected $fillable = [
        'title', 'description', 'order'
    ];

    public function parent() {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function threads() {
        return $this->hasMany('App\Thread', 'subcategory_id', 'id');
    }

    public function threadCount() {
        if(Auth::guest())
            $role = Role::where('name', '=', 'user')->first();
        else
            $role = Auth::user()->roles()->first();

        $specialPerm = $this->specialPermissionByRole($role->id)->first();

        if(!is_null($specialPerm) && $specialPerm->read_only_own) {
            if(Auth::guest()) {
                return 0;
            } else {
                return $this->threads()->where('user_id', '=', Auth::user()->steamid64)->count();
            }
        } else {
            return $this->threads()->count();
        }
    }

    public function latestThread() {
        if(Auth::guest())
            $role = Role::where('name', '=', 'user')->first();
        else
            $role = Auth::user()->roles()->first();

        $specialPerm = $this->specialPermissionByRole($role->id)->first();

        if(!is_null($specialPerm) && $specialPerm->read_only_own) {
            if(Auth::guest()) {
                return null;
            } else {
                $this->latest_thread = $this->threads()->where('user_id', '=', Auth::user()->steamid64)->orderBy('updated_at', 'desc')->first();
            }
        } else {
            if(is_null($this->latest_thread)) {
                $this->latest_thread = $this->threads()->orderBy('updated_at', 'desc')->first();
            }
        }

        return $this->latest_thread;
    }

    public function specialPermissions() {
        return $this->hasMany('App\SubCategoryRolePermission', 'subcategory_id', 'id');
    }

    public function specialPermissionByRole($role_id) {
        return $this->hasMany('App\SubCategoryRolePermission', 'subcategory_id', 'id')->where('role_id', '=', $role_id);
    }

    /**
     * Returns if the user has access to the subcategory
     * @return boolean
     */
    public function hasAccess() {
        if($this->access === null) {
            if(Auth::guest()) {
                $role = Role::where('name', '=', 'user')->first();
            } else {
                $role = Auth::user()->roles()->first();
            }

            if($this->specialPermissionByRole($role->id)->count() === 1) {
                $scrp = $this->specialPermissionByRole($role->id)->first();
            } else {
                $scrp = null;
            }

            if(is_null($scrp)) {
                $this->access = $role->hasPermission('view_threads');
            } else {
                $this->access = $scrp->read;
            }
        }

        return $this->access;
    }

    /**
     * Returns if the user is able to write a new thread
     * @return boolean
     */
    public function canCreateThread() {
        if($this->createThread === null) {
            if(Auth::guest()) {
                return false;
            } else {
                $role = Auth::user()->roles()->first();
            }

            if($this->specialPermissionByRole($role->id)->count() === 1) {
                $scrp = $this->specialPermissionByRole($role->id)->first();
            } else {
                $scrp = null;
            }

            if(is_null($scrp)) {
                $this->createThread = $role->hasPermission('post_new_threads');
            } else {
                $this->createThread = $scrp->create;
            }
        }

        return $this->createThread;
    }

    /**
     * Returns if the user can only see his own posts in this subcategory
     * @return boolean
     */
    public function canOnlySeeOwn() {
        if($this->seeOwn === null) {
            if(Auth::guest()) {
                $role = Role::where('name', '=', 'user')->first();
            } else {
                $role = Auth::user()->roles()->first();
            }

            if($this->specialPermissionByRole($role->id)->count() === 1) {
                $scrp = $this->specialPermissionByRole($role->id)->first();
            } else {
                $scrp = null;
            }

            if(is_null($scrp)) {
                $this->seeOwn = !$role->hasPermission('view_threads');
            } else {
                $this->seeOwn = $scrp->read_only_own;
            }
        }

        return $this->seeOwn;
    }

    /**
     * Returns if the user can reply to threads in this categories
     */
    public function canReplyToThreads() {
        if($this->replyToThread === null) {
            if(Auth::guest()) {
                return false;
            } else {
                $role = Auth::user()->roles()->first();
            }

            if($this->specialPermissionByRole($role->id)->count() === 1) {
                $scrp = $this->specialPermissionByRole($role->id)->first();
            } else {
                $scrp = null;
            }

            if(is_null($scrp)) {
                $this->replyToThread = $role->hasPermission('post_replies');
            } else {
                $this->replyToThread = $scrp->reply;
            }
        }

        return $this->replyToThread;
    }
}
