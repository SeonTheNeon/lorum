<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function author() {
        return $this->hasOne('App\User', 'steamid64', 'author_id');
    }

    public function type() {
        return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
    }

    public function messages() {
        return $this->hasMany('App\TicketMessage', 'ticket_id', 'id');
    }

    public function fields() {
        return $this->hasMany('App\TicketField', 'ticket_id', 'id');
    }

    public function additionalFields() {
        return $this->type->fields()->where('ticket_type_fields.type', '=', 'format_view')->get();
    }
}
