<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoryRolePermission extends Model
{
    protected $table = "subcategory_role_permissions";

    protected $fillable = [
        'subcategory_id', 'role_id', 'read',
        'read_only_own', 'write', 'reply'
    ];

    public function subCategory() {
        return $this->hasOne('App\SubCategory', 'id', 'subcategory_id');
    }

    public function role() {
        return $this->hasOne('App\Role', 'id', 'role_id');
    }
}
