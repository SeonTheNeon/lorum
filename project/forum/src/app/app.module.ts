import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
    { path: 'index', component: IndexComponent },
    { path: '', redirectTo: '/index', pathMatch: 'full'},
    { path: '**', component: PageNotFoundComponent }
];

import { Layout } from './layout.component';
import { IndexComponent } from './index/index.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CategoriesComponent } from './index/categories.component';

@NgModule({
    declarations: [
        Layout,
        IndexComponent,
        PageNotFoundComponent,
        CategoriesComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [Layout]
})
export class AppModule { }
