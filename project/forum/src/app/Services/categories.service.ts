/**
 * Created by max on 30.01.2017.
 */
import {Injectable, ApplicationRef} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from "rxjs";
import 'rxjs/Rx';
import {Category} from "../Models/category";

@Injectable()
export class CategoryService {

    constructor(private http:Http, private app: ApplicationRef) {
        this.http = http;
    }

    getCategories() : Observable<Category[]> {
        var tmp = this.http.get(this.app.components[0].instance.apiurl + 'api/categories')
            .map((res:Response) => res.json());
        return tmp;
    }

}