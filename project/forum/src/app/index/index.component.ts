import { Component, OnInit } from '@angular/core';
import { CategoryService } from "../Services/categories.service";
import { Category } from "../Models/category";
import {CategoriesComponent} from "./categories.component";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css'],
})

export class IndexComponent implements OnInit {

    constructor() { }

    ngOnInit() { }

}
