import {Component, OnInit, OnDestroy} from '@angular/core';
import { CategoryService } from "./Services/categories.service";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router, Params} from "@angular/router";
import {CookieService} from "angular2-cookie/services/cookies.service";

@Component({
    selector: 'body',
    templateUrl: 'layout.component.html',
    styleUrls: ['layout.component.css'],
    providers: [CategoryService, CookieService],
})

export class Layout implements OnInit, OnDestroy {

    public token: string;
    private sub: Subscription;

    apiurl = "http://api.lorum.vm/";
    loginurl = this.apiurl + "api/login";

    constructor (
        private route: ActivatedRoute,
        private router: Router,
        private _cookieService:CookieService
    ) {}

    ngOnInit(): void {
        this.sub = this.route.queryParams.subscribe((params: Params) => {
            this.token = params["token"];
        });

        console.log(this.token);
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

}
