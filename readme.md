# Lorum - 3kliksphilip Community Forum
## What the heck is this?
Lorum is a forum software built upon the Laravel web framework. The forum is integrated into steam and other
technologies like SourceBans, Kraken and other propietary software and plugins made for and by the 3kliksphilip
Community.

## What are our goals?
* Steam integration whereever it is possible
* Everything unified into one project. (SourceBans, Kraken, etc.)
* Automated Testing ;) (We love it!)

## You want to contribute? Do it!
Write @Zelenka on discord or just go to the issue list and do stuff!
If you want you can also invent the wheel and add some unwanted features or modify the layout!

But always include Tests for your new features.